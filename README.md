# Homework 3



##Driver's roles:
● Driver is able to register in the system;
● Driver is able to login into the system;
● Driver is able to view his profile info;
● Driver is able to change his account password;
● Driver is able to add trucks;
● Driver is able to view created trucks;
● Driver is able to assign truck to himself;
● Driver is able to update not assigned to him trucks info;
● Driver is able to delete not assigned to him trucks;
● Driver is able to view assigned to him load;
● Driver is able to interact with assigned to him load.

##Shipper's roles:
● Shipper is able to register in the system;
● Shipper is able to login into the system;
● Shipper is able to view his profile info;
● Shipper is able to change his account password;
● Shipper is able to delete his account;
● Shipper is able to create loads in the system;
● Shipper is able to view created loads;
● Shipper is able to update loads with status ‘NEW';
● Shipper is able to delete loads with status 'NEW';
● Shipper is able to post a load;
● Shipper is able to view shipping info.
