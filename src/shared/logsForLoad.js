module.exports = function(logs, text) {
  return [
    ...logs,
    {
      message: text,
      time: new Date().toISOString(),
    },
  ];
};
