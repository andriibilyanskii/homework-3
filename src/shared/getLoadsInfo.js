/**
 * @constructor
 * @param {Array} loads
 */
function getLoadsInfo(loads) {
  return loads.map((e) => {
    if (!e.logs) {
      e.logs = [];
    }
    return {
      _id: e._id,
      created_by: e.created_by,
      assigned_to: e.assigned_to,
      status: e.status,
      state: e.state,
      name: e.name,
      payload: e.payload,
      pickup_address: e.pickup_address,
      delivery_address: e.delivery_address,
      dimensions: e.dimensions,
      logs: e.logs.map((log) => ({message: log.message, time: log.time})),
      created_date: e.created_date,
    };
  });
}

module.exports = getLoadsInfo;
