const Users = require('../../model/users');
const {errorObj} = require('../../model/error');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function profileInfo(req, res) {
  try {
    if (!req.user.email) {
      throw new Error();
    }

    const user = await Users.findOne({email: req.user.email});
    if (user) {
      res.status(200).send({
        user: {
          _id: user._id,
          role: user.role,
          email: user.email,
          created_date: user.created_date,
        },
      });
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = profileInfo;
