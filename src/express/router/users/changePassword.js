const {errorObj} = require('../../model/error');
const Credentials = require('../../model/credentials');
const RegistrationCredentials = require('../../model/registrationCredentials');
const comparePassword = require('../../../shared/comparePassword');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Users = require('../../model/users');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function changePassword(req, res) {
  try {
    const {oldPassword, newPassword} = req.body;

    if (!oldPassword || !newPassword) {
      throw new Error();
    }

    const user = await RegistrationCredentials.findOne({email: req.user.email});
    const user1 = await Users.findOne({email: req.user.email});
    if (user && !user1.assignedLoad) {
      if (comparePassword(oldPassword, user.password)) {
        const encryptedPassword = await bcrypt.hash(newPassword, 10);
        const token = jwt.sign(
            {
              email: user.email,
              password: newPassword,
              role: user.role,
            },
            process.env.TOKEN_KEY,
            {
              expiresIn: '2h',
            },
        );
        await Credentials.updateOne(
            {email: req.user.email},
            {$set: {password: encryptedPassword, token: token}},
        );
        await RegistrationCredentials.updateOne(
            {email: req.user.email},
            {$set: {password: encryptedPassword, token: token}},
        );
        res.status(200).send({message: 'Password changed successfully'});
      } else {
        res.status(400).send(errorObj);
      }
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = changePassword;
