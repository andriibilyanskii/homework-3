const express = require('express');
const getLoad = require('./loads/getLoad');
const getLoads = require('./loads/getLoads');
const createLoad = require('./loads/createLoad');
const updateLoad = require('./loads/updateLoad');
const deleteLoad = require('./loads/deleteLoad');
const postLoad = require('./loads/postLoad');
const shippingInfo = require('./loads/shippingInfo');
const getActiveLoad = require('./loads/getActiveLoad');
const changeLoadState = require('./loads/changeLoadState');
const router = new express.Router();

router.get('/api/loads', (req, res) => {
  getLoads(req, res);
});

router.post('/api/loads', (req, res) => {
  createLoad(req, res);
});

router.get('/api/loads/active', (req, res) => {
  getActiveLoad(req, res);
});

router.patch('/api/loads/active/state', (req, res) => {
  changeLoadState(req, res);
});

router.get('/api/loads/:id', (req, res) => {
  getLoad(req, res);
});

router.put('/api/loads/:id', (req, res) => {
  updateLoad(req, res);
});

router.delete('/api/loads/:id', (req, res) => {
  deleteLoad(req, res);
});

router.post('/api/loads/:id/post', (req, res) => {
  postLoad(req, res);
});

router.get('/api/loads/:id/shipping_info', (req, res) => {
  shippingInfo(req, res);
});

module.exports = router;
