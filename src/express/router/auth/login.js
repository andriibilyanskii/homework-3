const jwt = require('jsonwebtoken');
const comparePassword = require('../../../shared/comparePassword');
const {errorObj} = require('../../model/error');
const RegistrationCredentials = require('../../model/registrationCredentials');
const validateUser = require('./validateUser');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function login(req, res) {
  try {
    const {email, password} = req.body;

    if (
      !email ||
      !password ||
      validateUser({email: email, password: password}).error
    ) {
      throw new Error();
    }

    const user = await RegistrationCredentials.findOne({email: email});
    let comparedPassword = null;
    try {
      comparedPassword = comparePassword(password, user.password);
    } catch (e) {}
    if (user && comparedPassword) {
      const token = jwt.sign(
          {
            user_id: user._id,
            email: email,
            password: password,
            role: user.role,
          },
          process.env.TOKEN_KEY,
          {
            expiresIn: '2h',
          },
      );
      user.token = token;
      res.status(200).send({
        jwt_token: token,
      });
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = login;
