const Users = require('../../model/users');
const {errorObj} = require('../../model/error');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Credentials = require('../../model/credentials');
const RegistrationCredentials = require('../../model/registrationCredentials');
const validateUser = require('./validateUser');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function register(req, res) {
  try {
    const {email, password, role} = req.body;

    if (
      !email ||
      !password ||
      !role ||
      validateUser({email: email, password: password, role: role}).error
    ) {
      throw new Error();
    }

    const users = await Users.find({email: email});
    if (users.length === 0) {
      const encryptedPassword = await bcrypt.hash(password, 10);
      const token = jwt.sign(
          {
            email: email,
            password: password,
            role: role,
          },
          process.env.TOKEN_KEY,
          {
            expiresIn: '2h',
          },
      );

      const credential = new Credentials({
        email,
        password: encryptedPassword,
        token: token,
      });
      const registrationCredentials = new RegistrationCredentials({
        email,
        password: encryptedPassword,
        token: token,
        role,
      });
      const user = new Users({
        role,
        email,
        created_date: new Date().toISOString(),
      });
      await credential.save();
      await registrationCredentials.save();
      await user
          .save()
          .then(() =>
            res.status(200).send({message: 'Profile created successfully'}),
          );
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = register;
