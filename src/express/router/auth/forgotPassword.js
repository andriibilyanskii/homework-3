const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Credentials = require('../../model/credentials');
const {errorObj} = require('../../model/error');
const sendmail = require('sendmail')();

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function forgotPassword(req, res) {
  try {
    const {email} = req.body;

    if (!email) {
      throw new Error();
    }

    const user = await Credentials.findOne({email: email});
    if (user) {
      const newPassword = Math.random().toString(36).slice(-8);

      sendmail(
          {
            from: 'andriibilyanskii@gmail.com',
            to: email,
            subject: 'Forgotten password',
            html: 'New password: ' + newPassword,
          },
          function(err, reply) {
            res.status(500).send(errorObj);
            return;
          },
      );

      const encryptedPassword = await bcrypt.hash(newPassword, 10);
      const token = jwt.sign(
          {
            email: user.email,
            password: newPassword,
          },
          process.env.TOKEN_KEY,
          {
            expiresIn: '2h',
          },
      );
      await Credentials.updateOne(
          {email: email},
          {$set: {password: encryptedPassword, token: token}},
      );

      res.status(200).send({
        message: 'New password sent to your email address',
      });
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = forgotPassword;
