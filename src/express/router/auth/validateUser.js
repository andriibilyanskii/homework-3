const Joi = require('joi');
/**
 * @constructor
 * @param {Object} user - user
 */
function validateUser(user) {
  const JoiSchema = Joi.object({
    email: Joi.string().email().min(1).max(30).required(),
    password: Joi.string().min(1).max(50).required(),
    role: Joi.string().valid('DRIVER', 'SHIPPER', 'driver', 'shipper'),
  }).options({abortEarly: false});

  return JoiSchema.validate(user);
}

module.exports = validateUser;
