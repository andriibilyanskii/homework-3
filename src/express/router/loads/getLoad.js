const Loads = require('../../model/loads');
const {errorObj} = require('../../model/error');
const getLoadsInfo = require('../../../shared/getLoadsInfo');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function getLoad(req, res) {
  try {
    const id = req.params.id;

    if (!id || !Loads) {
      throw new Error();
    }

    const loads = await Loads.find({_id: id});
    if (loads.length === 0) {
      res.status(400).send(errorObj);
    } else {
      res.status(200).send({
        load: getLoadsInfo(loads)[0],
      });
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = getLoad;
