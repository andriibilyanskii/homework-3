const Loads = require('../../model/loads');
const Users = require('../../model/users');
const loadState = require('./loadState');
const {errorObj} = require('../../model/error');
const logsForLoad = require('../../../shared/logsForLoad');
const Trucks = require('../../model/truck');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function changeLoadState(req, res) {
  try {
    if (!Loads || req.user.role !== 'DRIVER') {
      throw new Error();
    }

    const driver = await Users.findOne({
      email: req.user.email,
    });

    const load = await Loads.findOne({_id: driver.assignedLoad});

    const loadStateArray = Object.values(loadState);
    const index = loadStateArray.indexOf(load.state);
    const nextState = loadStateArray[index + 1];

    if (!nextState) {
      res.status(400).send(errorObj);
      return;
    }

    if (!load) {
      res.status(400).send(errorObj);
    } else {
      if (nextState !== loadState.arrivedDelivery) {
        await Loads.updateOne(
            {_id: load._id},
            {
              $set: {
                state: nextState,
                logs: logsForLoad(load.logs,
                    'Status is changed to ' + nextState),
              },
            },
        );
      } else {
        await Loads.updateOne(
            {_id: load._id},
            {
              $set: {
                state: nextState,
                status: 'SHIPPED',
                logs: logsForLoad(load.logs,
                    'Status is changed to ' + nextState),
              },
            },
        );
        await Trucks.updateOne(
            {_id: driver.assignedTruck},
            {$set: {status: 'IS'}},
        );
        await Users.updateOne({_id: driver._id}, {$set: {assignedLoad: ''}});
      }
      res.status(200).send({
        message: 'Load state changed to ' + nextState,
      });
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = changeLoadState;
