const Loads = require('../../model/loads');
const {errorObj} = require('../../model/error');
const getLoadsInfo = require('../../../shared/getLoadsInfo');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function getLoads(req, res) {
  const status = req.query.status || '';
  const limit = +req.query.limit || 10;
  const offset = +req.query.offset || 0;
  try {
    if (!Loads || limit > 50) {
      throw new Error();
    }

    let loads;
    if (req.user.role.toUpperCase() === 'DRIVER') {
      loads = await Loads.find({
        assigned_to: req.user.user_id,
        status: /ASSIGNED|SHIPPED/gi,
      })
          .skip(offset)
          .limit(limit);
    } else if (req.user.role.toUpperCase() === 'SHIPPER') {
      // {status: /NEW|POSTED/gi}
      loads = await Loads.find({created_by: req.user.user_id})
          .skip(offset)
          .limit(limit);
    } else {
      throw new Error();
    }
    if (status !== '') {
      loads = loads.filter((e) => e.status === status);
    }

    // if (loads.length === 0) {
    //   res.status(400).send(errorObj);
    // } else {
    res.status(200).send({
      loads: getLoadsInfo(loads),
    });
    // }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = getLoads;
