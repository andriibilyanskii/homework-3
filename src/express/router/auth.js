const express = require('express');
const forgotPassword = require('./auth/forgotPassword');
const login = require('./auth/login');
const register = require('./auth/register');
const router = new express.Router();

router.post('/api/auth/register', (req, res) => {
  register(req, res);
});

router.post('/api/auth/login', (req, res) => {
  login(req, res);
});

router.post('/api/auth/forgot_password', (req, res) => {
  forgotPassword(req, res);
});

module.exports = router;
