const Trucks = require('../../model/truck');
const Users = require('../../model/users');
const {errorObj} = require('../../model/error');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function deleteTruck(req, res) {
  try {
    const id = req.params.id;

    if (!id || req.user.role.toUpperCase() !== 'DRIVER') {
      throw new Error();
    }

    const truck = await Trucks.findOne({_id: id});
    if (truck) {
      await Trucks.deleteOne({_id: id});
      await Users.updateOne(
          {email: req.user.email},
          {$set: {assignedTruck: ''}},
      );
      res.status(200).send({message: 'Truck deleted successfully'});
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = deleteTruck;
