const Trucks = require('../../model/truck');
const validateTruckType = require('./validateTruckType');
const {errorObj} = require('../../model/error');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function updateInfo(req, res) {
  try {
    const id = req.params.id;
    const type = req.body.type;

    if (
      !id ||
      !type ||
      req.user.role.toUpperCase() !== 'DRIVER' ||
      validateTruckType({type: type}).error
    ) {
      throw new Error();
    }

    const truck = await Trucks.findOne({_id: id});
    if (truck) {
      await Trucks.updateOne({_id: id}, {$set: {type: type}});
      res.status(200).send({message: 'Truck details changed successfully'});
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = updateInfo;
