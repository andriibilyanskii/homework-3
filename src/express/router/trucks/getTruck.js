const Trucks = require('../../model/truck');
const {errorObj} = require('../../model/error');
const getTrucksInfo = require('../../../shared/getTruckInfo');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function getTruck(req, res) {
  const id = req.params.id;
  try {
    if (!Trucks || req.user.role.toUpperCase() !== 'DRIVER') {
      throw new Error();
    }

    const trucks = await Trucks.find({_id: id, created_by: req.user.user_id});
    if (trucks.length === 0) {
      res.status(400).send(errorObj);
    } else {
      res.status(200).send({
        truck: getTrucksInfo(trucks)[0],
      });
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = getTruck;
