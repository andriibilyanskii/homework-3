const Trucks = require('../../model/truck');
const {errorObj} = require('../../model/error');
const Users = require('../../model/users');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function assignTruck(req, res) {
  const id = req.params.id;

  try {
    if (req.user.role.toUpperCase() !== 'DRIVER') {
      throw new Error();
    }

    const truck = await Trucks.findOne({
      _id: id,
    });

    const driver = await Users.findOne({email: req.user.email});

    if (truck && !truck.assigned_to) {
      if (driver.assignedTruck) {
        await Trucks.updateOne(
            {_id: driver.assignedTruck},
            {$set: {assigned_to: ''}},
        );
      }
      await Trucks.updateOne(
          {_id: id},
          {$set: {assigned_to: req.user.user_id}},
      );
      await Users.updateOne(
          {email: req.user.email},
          {$set: {assignedTruck: truck._id}},
      );
      res.status(200).send({
        message: 'Truck assigned successfully',
      });
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = assignTruck;
