const Trucks = require('../../model/truck');
const validateTruckType = require('./validateTruckType');
const {errorObj} = require('../../model/error');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function addTruck(req, res) {
  try {
    const {type} = req.body;

    if (
      req.user.role.toUpperCase() !== 'DRIVER' ||
      validateTruckType({type: type}).error
    ) {
      throw new Error();
    }

    if (!type) {
      res.status(400).send(errorObj);
      return;
    }
    const truck = new Trucks({
      created_by: req.user.user_id,
      assigned_to: '',
      type: type,
      status: 'IS',
      created_date: new Date().toISOString(),
    });
    await truck.save().then(() =>
      res.status(200).send({
        message: 'Truck created successfully',
      }),
    );
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = addTruck;
