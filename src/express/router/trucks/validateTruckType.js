const Joi = require('joi');
/**
 * @constructor
 * @param {Object} truck - truck
 */
function validateTruckType(truck) {
  const JoiSchema = Joi.object({
    type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),
  }).options({abortEarly: false});

  return JoiSchema.validate(truck);
}

module.exports = validateTruckType;
