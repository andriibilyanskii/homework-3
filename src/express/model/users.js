const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
  role: {
    type: String,
    require: true,
    trim: true,
  },
  email: {
    type: String,
    require: true,
    trim: true,
  },
  created_date: {
    type: String,
    require: true,
    trim: true,
  },
  assignedTruck: {
    type: String,
    require: false,
  },
  assignedLoad: {
    type: String,
    require: false,
  },
});

const Users = mongoose.model('User', usersSchema);

module.exports = Users;
