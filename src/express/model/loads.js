const mongoose = require('mongoose');

const loadsSchema = new mongoose.Schema({
  created_by: {
    type: String,
    require: true,
    trim: true,
  },
  assigned_to: {
    type: String,
    require: true,
    trim: true,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    require: true,
    trim: true,
  },
  state: {
    type: String,
    enum: [
      '',
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery',
    ],
    require: true,
    trim: true,
  },
  name: {
    type: String,
    require: true,
    trim: true,
  },
  payload: {
    type: Number,
    require: true,
    trim: true,
  },
  pickup_address: {
    type: String,
    require: true,
    trim: true,
  },
  delivery_address: {
    type: String,
    require: true,
    trim: true,
  },
  dimensions: {
    type: {
      width: {type: Number},
      length: {type: Number},
      height: {type: Number},
    },
    require: true,
    trim: true,
  },
  logs: {
    type: [
      {
        message: {type: String},
        time: {type: String},
      },
    ],
    require: true,
    trim: true,
  },
  created_date: {
    type: String,
    require: true,
    trim: true,
  },
});

const Notes = mongoose.model('Load', loadsSchema);

module.exports = Notes;
