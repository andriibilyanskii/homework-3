const mongoose = require('mongoose');

const registrationCredentialsSchema = new mongoose.Schema({
  email: {
    type: String,
    require: true,
    trim: true,
  },
  password: {
    type: String,
    require: true,
    trim: false,
  },
  token: {
    type: String,
    trim: true,
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    require: true,
    trim: true,
  },
});

const RegistrationCredentials = mongoose.model(
    'RegistrationCredentials',
    registrationCredentialsSchema,
);

module.exports = RegistrationCredentials;
